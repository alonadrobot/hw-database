package com.DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class sql {
    public static void main(String[] args) {
        try {
            String url = "jdbc:mysql://localhost:3306/mySql";
            String user = "root";
            String password = "root";
            Connection con = DriverManager.getConnection(url, user, password);

            if (con != null) {
                System.out.println("Successful");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
