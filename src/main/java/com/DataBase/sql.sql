CREATE DATABASE mySql;
USE mySql;

CREATE TABLE mySql.Street (
                                      Id int(11) NOT NULL AUTO_INCREMENT,
                                      Name varchar(20) DEFAULT NULL,
                                      PRIMARY KEY (Id)
);

CREATE TABLE mySql.Persons (
                                     Id int(11) NOT NULL AUTO_INCREMENT,
                                     FirstName varchar(45) NOT NULL,
                                     LastName varchar(45) NOT NULL,
                                     Age int(11) NOT NULL,
                                     Id_Street int(11) DEFAULT NULL,
                                     PRIMARY KEY (Id)
);

ALTER TABLE Persons
    ADD FOREIGN KEY (Id_Street)
        REFERENCES street(Id);

INSERT INTO Persons (Id, FirstName, LastName, Age, Id_Street) VALUES
(1, 'Sasha', 'Ivanov', 22, 1),
(2, 'Vasa', 'Pupkin', 24, 2),
(3, 'Alona', 'Drobot', 22, 4),
(4, 'Dima', 'Pupkin', 33, null),
(5, 'Eva', 'Naomi', 13, 3),
(6, 'Jony', 'Dep', 51, 5),
(7, 'Karina', 'Kross', 31, 1),
(8, 'Vova', 'Kuprin', 56, null),
(9, 'Andrey', 'Karpov', 11, 1);
INSERT INTO street (Id, Name) VALUES
                                                                  (1, 'Puskinskaya'),
                                                                  (2, 'Mayakovskogo'),
                                                                  (3, 'Pravdi'),
                                                                  (4,'Nauki'),
                                                                  (5,'Holoseevskaya');
SELECT COUNT(*) FROM Persons;

SELECT AVG(Age) AS AgeAvg FROM Persons;

SELECT DISTINCT LastName FROM Persons ORDER BY LastName;

SELECT LastName, COUNT(*) FROM Persons GROUP BY LastName;

SELECT LastName FROM Persons WHERE LastName LIKE '_%b%_';

SELECT * FROM Persons WHERE id_street IS NULL;

SELECT * FROM Persons JOIN street ON Persons.id_street = street.id
WHERE street.name LIKE '%Pravdi%' AND Persons.age < 18;

SELECT street.id, street.Name, COUNT(Persons.id) FROM street JOIN Persons ON street.id = Persons.id_street
GROUP BY street.id, street.name;

SELECT * FROM street WHERE LENGTH(name) = 6;

SELECT street.id, street.name FROM street JOIN Persons ON street.id = Persons.id_street
GROUP BY street.id, street.name
HAVING COUNT(Persons.id) < 3;